import { ChakraProvider } from "@chakra-ui/react";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Home from "./component/Home";
import "./App.css";
import BoardList from "./component/BoardList";

function App() {
  return (
    <ChakraProvider>
    <Router>
    <div className="App">
        <Home />
      </div>
      <Switch>
        <Route path="/boards" component={BoardList} />
      </Switch>
      
    </Router>
    
    </ChakraProvider>
  );
}

export default App;
