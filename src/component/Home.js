import React, { Component } from "react";
import {Link} from 'react-router-dom'
import { Box, Flex, Heading, Button } from "@chakra-ui/react";
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
        <>
      <Flex>
      <Box p="3" bg="#026aa7" w="100%" border="apx solid green" borderRadius="3">
          <Button color="white" bg="hsla(0,0%,100%,.3)" fontSize="16px"><Link to="/boards">Boards</Link></Button>
        </Box>
        <Box p="3" bg="#026aa7" w="100%">
          <Button color="white" bg="hsla(0,0%,100%,.3)" fontSize="16px">Trello</Button>
        </Box>
        <Box p="3" bg="#026aa7" w="100%">
          <Button color="white" bg="hsla(0,0%,100%,.3)" fontSize="26px">+</Button>
        </Box>
      </Flex>
      {/* <BoardList/> */}
      </>
    );
  }
}

export default Home;
