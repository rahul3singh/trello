import React, { Component } from "react";
import { Box, Flex, Heading } from "@chakra-ui/react";
class BoardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardList: null,
    };
  }
  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/me/boards?key=8fc1e95f5ce7175f741c5d7e8aad94bf&token=bca4c498e73927b76ac38b075a47542e64a4331cf47b6de81b78e61db5b92ded`
    )
      .then((board) => board.json())
      .then((board) => this.setState({ boardList: board }));
  }
  render() {
    console.log(this.state.boardList);
    return (
      <Flex mt="20px" ml="40vw"  wrap="wrap">
        {this.state.boardList &&
          this.state.boardList.map((board) => {
            return (
              <Box
                key={board.id}
                width="15vw"
                height="15vh"
                position="relative"
                backgroundImage="url('https://trello-backgrounds.s3.amazonaws.com/SharedBackground/480x320/6c867e7205be5e150f4cb19b8e02b6b5/photo-1606773754386-1a6fb741841a.jpg')"
                backgroundPosition="0%"
                backgroundSize="cover"
                objectFit="contain"
               marginLeft="1vw"
               mb="20px"
              >
                <Box height="80%" width="80%" margin="auto" mt="10px">
                  <Heading fontSize="16px" color="white" mr="0">{board.name} </Heading>
                </Box>

                {console.log(board.url)}
              </Box>
            );
          })}
      </Flex>
    );
  }
}

export default BoardList;
